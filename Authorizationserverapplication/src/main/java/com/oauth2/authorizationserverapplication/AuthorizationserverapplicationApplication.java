package com.oauth2.authorizationserverapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@EnableResourceServer
public class AuthorizationserverapplicationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthorizationserverapplicationApplication.class, args);
    }

}
