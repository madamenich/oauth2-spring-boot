package com.oauth2.oktaoauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OktaoauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(OktaoauthApplication.class, args);
    }

}
